# janky

Launch URLs in browsers/incognito/profile of your choice.
It's intended for people who use separate browsers for work and personal stuff.

At this time it does not parse URLs, only hostnames.  
It also doesn't wildcard anything.
i.e. https://www.servergno.me/blah will be matched for config line "www.servergno.me", but wont't be matched for "servergno.me".  
This is a deliberate omission and will be implemented at a later time.

### Installation
- go get gitlab.com/horzadome/janky or download a binary and put it in your $PATH
  - if running from source, use ```glide install -v``` to get dependencies
- Create a config file ```$XDG_CONFIG_HOME/janky/janky.yaml``` .
- Set janky as default browser so it gets all URLs that you launch
- go crazy in config.yaml
- Example config:
  ```yaml
    # Profile names are completely arbitrary, they
    # just have to match section names in urls.
    # "exe" is mandatory, "args" and "env" are not.
    ---
    profiles:
      default:
        exe: /usr/bin/brave-nightly
        env: BRAVE_FLAGS=--profile-directory=Default
      work:
        exe: /usr/bin/google-chrome-stable
        args: --profile-directory=Profile 1
      work-nocookies:
        exe: google-chrome-stable
        args: --incognito
      personal:
        exe: /usr/bin/brave-nightly
        env: BRAVE_FLAGS=--profile-directory=Default
      personal-incognito:
        exe: brave-nightly
        env: BRAVE_FLAGS=--incognito --profile-directory=Profile 3

    # URLs have to be in profiles that you previously defined.
    # remember, domain.tld is not the same as host.domain.tld
    urls:
      work:
        - github.com
        - datadoghq.com
      work-nocookies:
        - servergno.me
      personal:
        - horza.org
        - facebook.com
      personal-incognito:
        - thepiratebay.org
        - rarbg.to
  ```
