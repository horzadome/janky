package main

import (
    "fmt"
    "os"
    "gopkg.in/urfave/cli.v2"
    . "github.com/logrusorgru/aurora"
    "gitlab.com/horzadome/janky/src/lib"
)

func main() {
    app_name := "janky"
    app_dir := janky.JoinStr(os.Getenv("XDG_CONFIG_HOME"),"/",app_name,"/")
    app_config := janky.JoinStr(app_dir,app_name,".yaml")

    cli.VersionFlag = &cli.BoolFlag{
      Name: "print-version", Aliases: []string{"V"},
      Usage: "print only the version",
    }

    app := &cli.App{
      Name: "janky",
      Version: "v0.5",
      Usage: "launch a browser based on domain",
      Action: func(c *cli.Context) error {

        if c.Args().Len() != 1 {
          fmt.Printf("%v\n", BgRed(White("Expected URL as argument")))
          os.Exit(1)
        }

        arg0 := c.Args().Get(0)
        if len(arg0) <= 3{
          fmt.Printf("%v\n", BgRed(White("Expected URL as argument")))
          os.Exit(1)
        }
        myurl := arg0
        var err error
        myhost := janky.ParseURL(myurl, err);

        ConfProfiles, err := janky.ListProfiles(app_config, err)
        fmt.Printf("Profiles : %v\n", ConfProfiles)

        BrowserExe, BrowserArgs, BrowserEnv := janky.GetProfile(app_config, "default", err)

        fmt.Printf("Defaults : %v | %v | %v\n", BrowserExe, BrowserArgs, BrowserEnv)

        var matchedit bool = false
        for _, profile := range ConfProfiles {
          profile := fmt.Sprintf("%v", profile)

          if profile != "default" {

            ConfUrls, err := janky.ListUrls(app_config, profile, err)
            for _, mydomain := range ConfUrls {
              mydomain := fmt.Sprintf("%v", mydomain)
              fmt.Printf("Checking : %v against %v\n", mydomain, myhost)
              if janky.MatchURL(mydomain,myhost) == true {
                matchedit = true
                BrowserExe, BrowserArgs, BrowserEnv = janky.GetProfile(app_config, profile, err)
              }
            }
          }
        }
        if !matchedit {
          fmt.Printf("%v\n", BgYellow(Black("\nURL not found in domain list, using default browser")))
        }
        fmt.Printf("%v \n", BgBlack(Green("URL found, launching " + BrowserEnv + "|" + BrowserExe + "|" + BrowserArgs + "|" + myurl + "\n")) )
        janky.LaunchBrowser(BrowserArgs, BrowserEnv, BrowserExe, myurl, err)
        os.Exit(0)
        return nil
      },
    }
    app.Run(os.Args)
}
