package janky

import (
    "os"
    "os/exec"
    "strings"
)

// string join function
func JoinStr(strs ...string) string {
	var sb strings.Builder
	for _, str := range strs {
		sb.WriteString(str)
	}
	return sb.String()
}

func SliceToStrMap(elements []string) map[string]string {
  elementMap := make(map[string]string)
  for _, s := range elements {
      elementMap[s] = s
  }
  return elementMap
}

// Elaborate way of launching a browser
func LaunchBrowser(BrowserArgs string, BrowserEnv string, BrowserExe string, myurl string, err error){
  cmd := exec.Command(BrowserExe, BrowserArgs, myurl )
	cmd.Env = append(os.Environ(),
            BrowserEnv,
  )
  cmd.Start()
}
