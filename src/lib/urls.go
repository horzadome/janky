package janky

import (
    "net/url"
)

// URL parser - put fancy regex here
func ParseURL(myurl string, err error) string{
  u, err := url.Parse(myurl)
  if err != nil {
      panic(err)
  }
  myhost := u.Hostname()
  return myhost
}

// Just a matcher, don't regex here
func MatchURL(mydomain string, myhost string) bool {
  if mydomain == myhost{
    return true
  } else {
    return false
  }
}
