package janky

import (
    "fmt"
    "github.com/smallfish/simpleyaml"
	  "io/ioutil"
    . "github.com/logrusorgru/aurora"
)

func ListProfiles(app_config string, err error) ([]string, error){
	cfg_source, err := ioutil.ReadFile(app_config)
	if err != nil {
    fmt.Printf("%v\n\n", BgRed(White("No config file. Create a conf file at " + app_config)))
    panic(err)
	}

	cfg, err := simpleyaml.NewYaml(cfg_source)
	if err != nil {
		panic(err)
	}

	ConfProfiles, err := cfg.Get("profiles").GetMapKeys()
	if err != nil {
    fmt.Printf("%v\n\n", BgRed(White("Can't get a list of profiles from " + app_config +"\nCheck config file's content")))
		panic(err)
	}
  return ConfProfiles, err
}

func GetProfile(app_config string, profile string, err error) (BrowserExe string, BrowserArgs string, BrowserEnv string){

	cfg_source, err := ioutil.ReadFile(app_config)
	if err != nil {
    fmt.Printf("%v\n\n", BgRed(White("No config file. Create a conf file at " + app_config)))
    panic(err)
	}

	cfg, err := simpleyaml.NewYaml(cfg_source)
	if err != nil {
		panic(err)
	}

  if CfgBrowserExe, err := cfg.Get("profiles").Get(profile).Get("exe").String(); err ==nil {
    BrowserExe = CfgBrowserExe
  }
  if CfgBrowserArgs, err := cfg.Get("profiles").Get(profile).Get("args").String(); err ==nil {
    BrowserArgs = CfgBrowserArgs
  }
  if CfgBrowserEnv, err := cfg.Get("profiles").Get(profile).Get("env").String(); err ==nil {
    BrowserEnv = CfgBrowserEnv
  }
  return BrowserExe, BrowserArgs, BrowserEnv
}

func ListUrls(app_config string, profile string, err error) ([]interface {}, error){

	cfg_source, err := ioutil.ReadFile(app_config)
	if err != nil {
    fmt.Printf("%v\n\n", BgRed(White("No config file. Create a conf file at " + app_config)))
    panic(err)
	}

	cfg, err := simpleyaml.NewYaml(cfg_source)
	if err != nil {
		panic(err)
	}

	ConfUrls, err := cfg.Get("urls").Get(profile).Array()
	if err != nil {
    fmt.Printf("%v\n\n", BgRed(White("Can't get a list of URLs from " + app_config +"\nCheck config file's content")))
		panic(err)
	}

  return ConfUrls, err
}
